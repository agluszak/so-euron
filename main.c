#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

extern uint64_t euron(uint64_t n, char const* prog);

uint64_t get_value(uint64_t n) {
    assert(n < N);
//    printf("get_value: %lu\n", n);
    return n + 1;
}

void put_value(uint64_t n, uint64_t v) {
    assert(n < N);
    assert(v == n + 4);
//    printf("put_value: %lu, %lu\n", n, v);
}

struct gowno {
    uint64_t n;
    char const* prog;
    uint64_t result;
};

void* call_euron(void* dupa) {
    struct gowno* dane = (struct gowno*) dupa;
    dane->result = euron(dane->n, dane->prog);
    return NULL;
}

int main() {
    char const* prog = "01234n+P56789E-+D+*G*1n-+S2ED+E1-+75+-BC";
    pthread_t first;
    pthread_t second;

    struct gowno first_args;
    first_args.n = 0;
    first_args.prog = prog;

    struct gowno second_args;
    second_args.n = 1;
    second_args.prog = prog;

    if (pthread_create(&first, NULL, call_euron, &first_args)) {
        fprintf(stderr, "Error creating first thread\n");
        return 1;
    }
    if (pthread_create(&second, NULL, call_euron, &second_args)) {
        fprintf(stderr, "Error creating second thread\n");
        return 1;
    }

    pthread_join(first, NULL);
    pthread_join(second, NULL);

    printf("first: %lu\nsecond: %lu", first_args.result, second_args.result);

    return 0;

}
