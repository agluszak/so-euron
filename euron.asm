global euron
extern get_value, put_value
section .bss
    ;sync resb N*N
    exchange resq N
section .data
    sync times N dq -1

section .text

; rsi - char const* prog
; rdi - uint64_t n

%define letter_number r12
%define m r13

euron:
    ; zachowujemy stan rejestrów
    push rbp
    push rbx
    push r12
    push r13
    push r14
    push r15
    mov rbp, rsp

    xor letter_number, letter_number
.next_letter:
    mov dl, [rsi + letter_number]
.plus:
    cmp dl, '+'
    jne .times
    pop rax
    pop rbx
    add rax, rbx
    push rax
    inc letter_number
    jmp .next_letter
.times:
    cmp dl, '*'
    jne .minus
    pop rax
    pop rbx
    mul rbx
    push rax
    inc letter_number
    jmp .next_letter
.minus:
    cmp dl, '-'
    jne .number
    pop rax
    neg rax
    push rax
    inc letter_number
    jmp .next_letter
.number:
    cmp dl, '0' ; sprawdzamy czy wczytany znak jest którąś z cyfr
    jl .n
    cmp dl, '9'
    jg .n
    movsx rax, dl
    sub rax, '0' ; zmieniamy znak na liczbę
    push rax
    inc letter_number
    jmp .next_letter
.n:
    cmp dl, 'n'
    jne .B
    push rdi
    inc letter_number
    jmp .next_letter
.B:
    cmp dl, 'B'
    jne .C
    pop rax
    pop rbx
    cmp rbx, 0
    je .B_no_move
    add letter_number, rax
.B_no_move:
    push rbx
    inc letter_number
    jmp .next_letter
.C:
    cmp dl, 'C'
    jne .D
    pop rax
    inc letter_number
    jmp .next_letter
.D:
    cmp dl, 'D'
    jne .E
    pop rax
    push rax
    push rax
    inc letter_number
    jmp .next_letter
.E:
    cmp dl, 'E'
    jne .G
    pop rax
    pop rbx
    push rax
    push rbx
    inc letter_number
    jmp .next_letter
.G:
    cmp dl, 'G'
    jne .P
    push rdi ; zachowujemy na stosie wartości, które może zmodyfikować
    push rsi ; wywołanie zewnętrznej funkcji
    call get_value
    pop rsi
    pop rdi
    push rax
    inc letter_number
    jmp .next_letter
.P:
    cmp dl, 'P'
    jne .S
    pop rax
    push rdi ; zachowujemy na stosie wartości, które może zmodyfikować
    push rsi ; wywołanie zewnętrznej funkcji
    mov rsi, rax
    call put_value
    pop rsi
    pop rdi
    inc letter_number
    jmp .next_letter
.S:
    cmp dl, 'S'
    jne .end_of_program
    pop m

    ; mój adres w tablicy
    mov r14, sync
    mov rax, rdi
    imul rax, N
    add r14, rax
    add r14, m
    ; adres partnera w tablicy
    mov r15, sync
    mov rax, m
    imul rax, N
    add r15, rax
    add r15, rdi
.busy_wait_in:
    mov rcx, [sync + rdi * 8];
    cmp rcx, -1
    jne .busy_wait_in
    pop rbx
    mov [exchange + rdi * 8], rbx
    mov [sync + rdi * 8], m
.busy_wait_out:
    mov rcx, [sync + m * 8] ;xchg
    cmp rcx, rdi
    jne .busy_wait_out
    mov rbx, [exchange + m * 8]
    push rbx
    mov QWORD [sync + m * 8], -1
    inc letter_number
    jmp .next_letter
.end_of_program:
    pop rax
    mov rsp, rbp
    ; przywracamy rejestry
    pop r15
    pop r14
    pop r13
    pop r12
    pop rbx
    pop rbp
    ret
