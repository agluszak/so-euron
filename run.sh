#!/usr/bin/env bash
nasm -DN=2 -f elf64 -F dwarf -g -o euron.o euron.asm
gcc -pthread -DN=2 -no-pie -g euron.o main.c
./a.out